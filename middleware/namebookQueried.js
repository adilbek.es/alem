export default function ({ route, redirect, app }) {
  if (route.name.startsWith('namebooks-id-edit')) {
    if (!(route.query && route.query.item)) {
      const path = app.localePath({
        name: 'namebooks-id-preview',
        params: { id: route.params.id }
      })
      return redirect(path)
    }
  }
}
