export default {
  async FETCH_BOOK ({ commit }, payload) {
    let url = 'books'
    if (payload.type) {
      url = payload.type
    }
    const { data } = await this.$axios.$get(url + '/' + payload.id, {
      params: {
        lang: payload.language
      }
    })
    commit('SET_BOOK', data)
  },
  SET_FORM ({ commit }, payload) {
    commit('SET_FORM', payload)
  },
  CLEAR_FORM ({ commit }) {
    commit('SET_FORM')
  },
  async FETCH_ADDONS ({ commit }) {
    const url = 'addons'
    const { data } = await this.$axios.$get(url)
    commit('SET_ADDONS', data)
  },
  SET_ACTIVE_ADDONS ({ commit }, payload) {
    commit('SET_ACTIVE_ADDONS', payload)
  },
  ADD_ACTIVE_ADDON ({ commit, state }, payload) {
    if (state.addons.filter(item => item.id === payload.id).length) {
      commit('ADD_ACTIVE_ADDON', payload)
    }
  },
  REMOVE_ACTIVE_ADDON ({ commit }, payload) {
    commit('REMOVE_ACTIVE_ADDON', payload)
  },
  REMOVE_ACTIVE_ADDONS ({ commit }) {
    commit('REMOVE_ACTIVE_ADDONS')
  }
}
