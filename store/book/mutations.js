export default {
  SET_BOOK (state, payload) {
    state.book = payload
  },
  SET_FORM (state, payload = null) {
    state.form = payload
  },
  SET_ADDONS (state, payload) {
    state.addons = payload
  },
  SET_ACTIVE_ADDONS (state, payload) {
    state.activeAddons = payload
  },
  ADD_ACTIVE_ADDON (state, payload) {
    state.activeAddons.push(payload)
  },
  REMOVE_ACTIVE_ADDON (state, payload) {
    state.activeAddons = state.activeAddons.filter(item => item.id !== payload.id)
  },
  REMOVE_ACTIVE_ADDONS (state) {
    state.activeAddons = []
  }
}
