import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = () => ({
  books: [],
  book: null,
  form: null,
  addons: [],
  activeAddons: [],
  expire: 48
})

export default {
  state,
  getters,
  mutations,
  actions
}
