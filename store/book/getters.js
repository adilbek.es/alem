export default {
  getBook (state) {
    return state.book
  },
  getForm (state) {
    return state.form
  },
  getLanguage (state) {
    return state.form ? state.form.language : 'ru'
  },
  getAddons (state) {
    return state.addons
  },
  getActiveAddons (state) {
    return state.activeAddons
  }
}
