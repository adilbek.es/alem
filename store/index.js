export const actions = {
  nuxtServerInit ({ dispatch, state }) {
    dispatch('page/FETCH_MENU')
    if (state.currency.currencies.length === 0) {
      dispatch('currency/FETCH_CURRENCIES')
    }
  }
}
