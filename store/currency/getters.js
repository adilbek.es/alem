export default {
  getCurrency (state) {
    return state.currency
  },
  getCurrencies (state) {
    return state.currencies
  }
}
