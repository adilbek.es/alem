export default {
  async FETCH_CURRENCIES ({ commit }, payload) {
    const { data } = await this.$axios.$get('currencies', {
      params: payload
    })
    commit('SET_CURRENCIES', data)
  },
  SET_CURRENCY ({ commit }, payload) {
    const countries = [
      {
        name: 'KAZ',
        kod: 'KZT'
      },
      {
        name: 'RUS',
        kod: 'RUB'
      },
      {
        name: 'UKR',
        kod: 'UAH'
      },
      {
        name: 'UZB',
        kod: 'UZS'
      },
      {
        name: 'USA',
        kod: 'USD'
      }
    ]

    const country = countries.find(item => item.name === payload || item.kod === payload)
    if (country) {
      commit('SET_CURRENCY', country.kod)
    } else {
      commit('SET_CURRENCY', 'USD')
    }
  }
}
