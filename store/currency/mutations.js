export default {
  SET_CURRENCIES (state, payload) {
    state.currencies = payload
  },
  SET_CURRENCY (state, payload) {
    state.currency = payload
  }
}
