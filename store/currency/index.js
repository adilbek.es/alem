import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = () => ({
  currencies: [],
  currency: '',
  expire: 12
})

export default {
  state,
  getters,
  mutations,
  actions
}
