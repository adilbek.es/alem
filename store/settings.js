export const state = () => ({
  websiteName: 'Alem',
  host: 'https://admin.alem.biz',
  locales: ['ru', 'kk'],
  locale: 'ru',
  phone: '+77713630202',
  mail: '',
  socials: {
    instagram: 'https://www.instagram.com/bizalembiz',
    facebook: 'https://www.facebook.com/Alem-Biz-106782354572251',
    whatsapp: 'https://wa.me/+77713630202'
  },
  address_ru: '<strong>г. Нур-Султан, Кабанбай батыр 48А, ЖК Времена года, Весна-2</strong><br />(Пн-пт: с 10:00 до 19:00, Сб-вс: выходной)',
  address_kk: '<strong>Нур-Султан қ., Қабанбай батыр 48А, Времена года ТК, Весна-2</strong><br />(Дс-жм: 10:00-ден 19:00-ге дейін, сб-жб: демалыс)',
  expire: 240
})

export const getters = {
  getWebsiteName (state) {
    return state.websiteName
  },
  getHostName (state) {
    return state.host
  },
  getPhone (state) {
    return state.phone
  },
  getMail (state) {
    return state.mail
  },
  getLocales (state) {
    return state.locales
  },
  getLocale (state) {
    return state.locale
  },
  getSocials (state) {
    return state.socials
  },
  getAddress (state, lang = 'ru') {
    return state['address_' + lang]
  }
}

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  }
}

export const actions = {
  SET_LANG ({ commit, route }, locale) {
    commit('SET_LANG', locale)
  }
}
