export default {
  SET_SERVICES (state, payload) {
    state.services = payload
  },
  SET_INFOBLOCKS (state, payload) {
    state.infoblocks = payload
  }
}
