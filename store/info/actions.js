export default {
  async FETCH_SERVICES ({ commit }, payload) {
    const { data } = await this.$axios.$get('services', {
      params: payload
    })
    commit('SET_SERVICES', data)
  },
  async FETCH_INFOBLOCKS ({ commit }, payload) {
    const { data } = await this.$axios.$get('infoblocks', {
      params: payload
    })
    commit('SET_INFOBLOCKS', data)
  }
}
