import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = () => ({
  services: [],
  infoblocks: []
})

export default {
  state,
  getters,
  mutations,
  actions
}
