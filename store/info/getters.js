export default {
  getServices (state) {
    return state.services
  },
  getInfoblocks (state) {
    return state.infoblocks
  }
}
