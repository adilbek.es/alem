export default {
  SET_PAGE (state, payload) {
    state.page = payload
  },
  SET_MENU (state, payload) {
    state.menu = payload
  }
}
