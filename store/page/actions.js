export default {
  async FETCH_PAGE ({ commit }, payload) {
    const { data } = await this.$axios.$get('pages/' + payload.slug)
    commit('SET_PAGE', data)
  },
  async FETCH_MENU ({ commit }) {
    const { data } = await this.$axios.$get('menu/footer')
    commit('SET_MENU', data)
  }
}
