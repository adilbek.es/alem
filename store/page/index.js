import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = () => ({
  page: null,
  menu: []
})

export default {
  state,
  getters,
  mutations,
  actions
}
