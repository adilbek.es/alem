export default {
  getPage (state) {
    return state.page
  },
  getMenu (state) {
    return state.menu
  }
}
