export default {
  ADD_TO_CART ({ commit }, payload) {
    commit('ADD_TO_CART', payload)
    commit('INCREMENT_ITEM_QUANTITY')
  },
  REMOVE_FROM_CART ({ commit }, payload) {
    commit('REMOVE_FROM_CART', payload)
    commit('DECREMENT_ITEM_QUANTITY')
  },
  UPDATE_CART_ITEM ({ commit }, payload) {
    commit('UPDATE_CART_ITEM', payload)
  },
  REMOVE_ADDON_FROM_CART ({ commit }, payload) {
    commit('REMOVE_ADDON_FROM_CART', payload)
  },
  CLEAR_CART ({ commit }) {
    commit('CLEAR_CART')
  }
}
