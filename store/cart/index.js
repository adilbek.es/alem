import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = () => ({
  items: [],
  itemQuantity: 0,
  expire: 24,
  orderInfo: null
})

export default {
  state,
  getters,
  mutations,
  actions
}
