import Vue from 'vue'
export default {
  INCREMENT_ITEM_QUANTITY (state) {
    state.itemQuantity++
  },
  DECREMENT_ITEM_QUANTITY (state) {
    state.itemQuantity--
  },
  ADD_TO_CART (state, payload) {
    state.items.push(payload)
  },
  CLEAR_CART (state) {
    state.items = []
    state.itemQuantity = 0
  },
  REMOVE_FROM_CART (state, payload) {
    state.items = state.items.filter(item => item.id !== payload.id)
  },
  REMOVE_ADDON_FROM_CART (state, payload) {
    const item = state.items.find(el => el.id === payload.id)
    item.addons = item.addons.filter(el => el.id !== payload.addonId)
  },
  UPDATE_CART_ITEM (state, payload) {
    const itemIndex = state.items.findIndex(el => el.id === payload.id)
    Vue.set(state.items, itemIndex, payload)
  }
}
