import { _ } from 'underscore/underscore'

export default {
  getItems (state) {
    return state.items
  },
  getItemQuantity (state) {
    return state.itemQuantity
  },
  getTotalItemQuantity (state) {
    const addons = state.items.map((item) => {
      return item.addons
    })

    const len = state.itemQuantity + _.flatten(addons).length
    return len
  },
  getTotalPrice (state) {
    const reducer = (accumulator, currentValue) => accumulator + currentValue.price
    let price = 0
    const itemsLength = state.items.length
    if (itemsLength > 1) {
      const products = _.flatten(state.items.map((item) => {
        return item.product
      }))
      price = products.reduce(reducer, 0)
    } else if (itemsLength === 1) {
      const product = state.items[0].product
      price = product.price
    }

    const addons = _.flatten(state.items.map((item) => {
      return item.addons
    }))

    const addonsLength = addons.length
    if (addonsLength > 1) {
      price = price + addons.reduce(reducer, 0)
    } else if (addonsLength === 1) {
      price = price + addons[0].price
    }
    return price
  }
}
