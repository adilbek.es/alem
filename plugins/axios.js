export default function ({ app, $axios, redirect }) {
  $axios.onRequest((config) => {
    config.headers.common['Accept-Language'] = app.i18n.locale || 'ru'
    // eslint-disable-next-line no-console
    console.log('Making request to ' + config.url)
  })

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}
