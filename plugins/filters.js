import Vue from 'vue'
import petrovich from 'petrovich'

import { _ } from 'underscore/underscore'
const vowels = require('~/static/json/vowels.json')
const consonants = require('~/static/json/consonants.json')
const endings = require('~/static/json/endings.json')

// vowel characters
const ALL_VOWELS = [...new Set(Object.values(_.flatten(Object.values(vowels))))]
const VOWELS_BOTH = vowels.both

// consonant characters
const ALL_CONSONTANTS = [...new Set(Object.values(_.flatten(Object.values(consonants))))]
const CONSONANTS_UNVOICED = consonants.unvoiced
const CONSONANTS_VOICED = consonants.voiced

Vue.filter('image', val => process.env.AXIOS_BASE_URL + '/storage/' + val)
Vue.filter('imageSize', (val, size) => {
  const splitted = val.split('.')
  const extension = splitted.pop()
  return splitted.join('.') + '-' + size + '.' + extension
})

Vue.filter('price', (val, key, format = true) => {
  const symbols = {
    KZT: '&#8376;',
    USD: '&#36;',
    RUB: '&#8381;',
    UZS: 'UZD',
    UAH: '&#8372;'
  }

  const formatter = new Intl.NumberFormat('ru-RU', {
    minimumFractionDigits: val % 1 === 0 ? 0 : 2
  })
  if (format) {
    return formatter.format(val) + symbols[key]
  }
  return parseFloat(val)
})

/**
 * @param {Object} object
 * @param {string} char
 */
function getKeyByValue (object, char) {
  return Object.keys(object).find((key) => {
    return object[key].includes(char)
  })
}

/**
 * @param {string} str
 * @param {Array} array
 */
function getLastIndex (str, array) {
  const positionIndex = -1
  for (const char of str.split('').reverse()) {
    if (array.includes(char)) {
      return str.lastIndexOf(char)
    }
  }

  return positionIndex
}

function getVowelType (str) {
  let indexVowel = getLastIndex(str, ALL_VOWELS)

  if (indexVowel > -1) {
    let type = getKeyByValue(vowels, str.charAt(indexVowel))
    if (VOWELS_BOTH.includes(str.charAt(indexVowel))) {
      const nextIndexVowel = getLastIndex(str.substring(0, indexVowel), ALL_VOWELS)
      if (indexVowel !== nextIndexVowel && nextIndexVowel > -1) {
        indexVowel = nextIndexVowel
        type = getKeyByValue(vowels, str.charAt(indexVowel))
      }
    }

    return type
  } else {
    return ''
  }
}

function decline (val) {
  let ending = endings.cases
  const type = getVowelType(val)

  if (type) {
    ending = Object.values(ending)
    ending = ending[2][type]
  } else {
    return val
  }

  const str = val
  val = val.toLowerCase()
  const lastChar = val[val.length - 1]
  if (ALL_CONSONTANTS.includes(lastChar)) {
    const kA = [...CONSONANTS_UNVOICED, ...CONSONANTS_VOICED].filter(el => !['ж', 'з'].includes(el))

    if (kA.includes(lastChar)) {
      ending = ending[1]
    } else {
      ending = ending[0]
    }
  } else {
    ending = ending[0]
  }
  return str + ending
}

Vue.filter('genitive', (val, lang, gender) => {
  if (lang === 'ru') {
    switch (gender) {
      case 'boy':
        gender = 'male'
        break
      case 'girl':
        gender = 'female'
        break
    }
    return petrovich[gender].first.genitive(val)
  } else if (lang === 'kk') {
    return decline(val)
  }
  return val
})

Vue.filter('declareNum', (titles, number) => {
  const cases = [2, 0, 1, 1, 1, 2]
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]]
})
