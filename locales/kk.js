export default () => {
  return new Promise(function (resolve) {
    resolve({
      seo: {
        title: 'Alem - әрбір балаға арналған персоналды кітаптар',
        siteName: 'Alemkids.kz',
        description: 'Әрбір балаға арналып бар ықыласпен әрі шын жүректен құрастылырылады, кітаптар сіздің балаңыз үшін ерекше, және де ең бастысы жоғары сапалы жасалады',
        keywords: 'кітаптар, балалар, балалар кітаптары, жеке кітаптар, ең жақсы кітаптар, балаларға арналған барлық заттар'
      },
      links: {
        home: 'Басты бет'
      },
      home: {
        title: 'Әр сәби туралы <br />ерекше кітап',
        subtitle: 'Балаңызға өзі туралы кітап сыйлаңыз'
      },
      cart: {
        name: 'Себет',
        buy_for: '{price}-ге сатып алу',
        order_summary: 'Жеткізу әдісі',
        items: [
          '{count} тауар',
          '{count} тауар',
          '{count} тауар'
        ],
        product_detail: 'Өнім туралы ақпарат',
        products_cost: 'Тауарлардың құны',
        total_cost: 'Жалпы құны',
        price: 'Бағасы',
        edit: 'Өңдеу',
        remove: 'Алып тастау',
        continue_shopping: 'Сатып алуды жалғастыру',
        personal_data: 'Жеке мәліметтер',
        shipping: 'Жеткізу',
        currency: 'Валюта',
        address: 'Адрес',
        pickup: 'Алып кету',
        checkout: 'Төлеу',
        checkout_credit: 'Бөліп төлеу',
        back: 'Артқа',
        empty_cart: 'Себетте ештеңе жоқ',
        pay_description: 'alem.biz сайтында тауар сатып алу',
        credit_description: 'alem.biz сайтында тауар сатып алу және бөліп төлеу',
        success_pay: 'Құттықтаймыз, {name} - Сіздің тапсырысыңыз сәтті төленді!',
        error_pay: 'Кешіріңіз, төлем жасау кезінде қате пайда болды. Өтінемін, кейінірек қайталап көріңіз немесе бізге келесі нөмірге қоңырау шалыңыз: <a href="tel:{phone}">{phone}</a>',
        guest: 'қонақ'
      },
      namebook: {
        subtitle: 'Ұлдар <br /> мен қыздарға арналған',
        list: [
          '0-ден 8 жасқа дейінгі балаларға арналған керемет сыйлық',
          'әр балада керемет әсер қалдыру',
          'Жасына байланысты кітабы - бала кезінен оқуға деген сүйіспеншілікті дамытады',
          'кітабын қатты мұқабада тапсырыс беруге болады'
        ],
        for: 'Арнайы <strong class="text-capitalize"> {name} </strong> арналған жеке кітап',
        book_for: '<span class="text-capitalize">{name}</span> арналған <strong>{book}</strong>',
        dedication_title: '{name} арналған алғы тілегіңізді жазыңыз (тегін)',
        dedication_text: 'Біз сіздің мәтініңізді кітаптың бірінші бетіне шығарамыз',
        dedication_left: [
          '{count} таңба қалды',
          '{count} таңба қалды',
          '{count} таңба қалды'
        ],
        addons: 'Қосымша',
        addons_title: 'Сиқырға айналдыр'
      },
      form: {
        name: 'Аты',
        surname: 'Тегі',
        email: 'E-mail',
        phone: 'Телефон',
        language: 'Тіл',
        view_book: 'Кітапты қарау',
        boy: 'Ұл',
        girl: 'Қыз',
        choose_character: 'Кітаптан өз кейіпкеріңізді таңдаңыз',
        dedication: 'Өз тілегіңді жаз',
        character: 'Кейіпкер таңда',
        street: 'Көше',
        city: 'Қала',
        country: 'Ел',
        zip_code: 'Пошта индексі',
        select_country: 'Елді таңдаңыз',
        promocode: 'Промокод'
      },
      validation: {
        fill_language: {
          text: 'Өрісті {lang} әріптермен толтырыңыз.',
          ru: 'орысша',
          kk: 'қазақша',
          latin: 'латын'
        },
        min: '{field} өрісі {count} таңбадан кем болмауы керек.',
        max: '{field} өрісі {count} таңбадан көп болмауы керек.',
        required: '{field} өрісін толтыру керек',
        required_character: 'Кейіпкеріңізді таңдаңыз.',
        string: '{field} өрісі жол болуы керек.'
      },
      languages: {
        ru: 'Русский',
        kk: 'Қазақша'
      },
      create_book: 'Кітапты құрастыру',
      copyright: '© Барлық құқықтар қорғалған.',
      continue: 'Жалғастыру',
      close: 'Жабу',
      edit: 'Өзгерту',
      share: 'Бөлісу',
      notice: {
        namebook: {
          title: '+1 бақытты бала',
          content: 'Тағы бір сиқырлы кітап өз иесін тапты!'
        }
      },
      create_look: 'Өз кітабыңды кұрастырып, тамашала!',
      payType: {
        once: 'қазір <span class="text-warning">{sum}</span> төлеу',
        credit: 'қазір <span class="text-warning">{sum}</span> төлеп, қалғанын <span class="text-warning">{months} айға</span> бөліп төлеу'
      },
      agree_oferta: '«Төлеу» батырмасын басу арқылы, сіз өзіңіздің кәмелеттік жаста екеніңізді және <a target="_blank" href="/kk/publichnaya-oferta">Қоғамдық офертаға</a> келісіміңізді растайсыз',
      promocode_status: {
        error: 'Промокод жарамсыз',
        success: 'Промокод жарамды'
      },
      installment: {
        title: 'Бөліп төлеу',
        button: '3 айға'
      },
      charity: 'Балалар қорына сіздің көмегіңіз',
      delivery_date: 'Тапсырыстың жеткізілу уақыты'
    }
    )
  })
}
