export default () => {
  return new Promise(function (resolve) {
    resolve({
      seo: {
        title: 'Alem - персонализированные книги для детей',
        siteName: 'Alemkids.kz',
        description: 'Персонализированные детские книги созданы уникально для вашего ребенка от всей души и, самое главное, в самом высоком качестве',
        keywords: 'книги, дети, детские книги, персонализированные книги, лучшие книги, все для детей'
      },
      links: {
        home: 'Главная'
      },
      home: {
        title: 'Персональные книги <br />для каждого малыша',
        subtitle: 'Подари детям полезную <br /> книгу про них'
      },
      cart: {
        name: 'Корзина',
        buy_for: 'Купить за {price}',
        order_summary: 'Способ доставки',
        items: [
          '{count} товар',
          '{count} товара',
          '{count} товаров'
        ],
        product_detail: 'Информация о товаре',
        products_cost: 'Стоимость товаров',
        total_cost: 'Общая стоимость',
        price: 'Цена',
        edit: 'Редактировать',
        remove: 'Удалить',
        continue_shopping: 'Продолжить покупку',
        personal_data: 'Личные данные',
        shipping: 'Доставка',
        currency: 'Валюта',
        address: 'Адрес',
        pickup: 'Самовывоз',
        checkout: 'Оплатить',
        checkout_credit: 'В рассрочку',
        back: 'Назад',
        empty_cart: 'В корзине ничего нет',
        pay_description: 'Покупка товаров на сайте alem.biz',
        credit_description: 'Рассрочка товаров на сайте alem.biz',
        success_pay: 'Поздравляю, {name} - Ваш заказ был успешно оплачен!',
        error_pay: 'Извините, при попытке оплаты произошла ошибка. Пожалуйста, ' +
          'повторите попытку позже или позвоните нам на этот номер <a href="tel:{phone}">{phone}</a>',
        guest: 'гость'
      },
      namebook: {
        subtitle: 'Для мальчиков <br />и девочек',
        list: [
          'прекрасный подарок для детей от 0 до 8 лет',
          'вау-эффект у каждого ребенка, который находит своё имя',
          'книга на вырост - развивает любовь к чтению с самого детства',
          'книгу можно заказать в твёрдой обложке'
        ],
        for: 'Персональная книга специально для <strong class="text-capitalize">{name}</strong>',
        book_for: '<strong>{book}</strong> для <span class="text-capitalize">{name}</span>',
        dedication_title: 'Напишите свое пожелание для {name} (бесплатно)',
        dedication_text: 'Мы напечатаем ваш текст на первой странице книги',
        dedication_left: [
          'Остался {count} символ',
          'Осталось {count} символа',
          'Осталось {count} символов'
        ],
        addons: 'Дополнительно',
        addons_title: 'Заверни это в магию'
      },
      form: {
        name: 'Имя',
        surname: 'Фамилия',
        email: 'E-mail',
        phone: 'Телефон',
        language: 'Язык',
        view_book: 'Посмотреть книгу',
        boy: 'Мальчик',
        girl: 'Девочка',
        choose_character: 'Выбери своего героя в книге',
        dedication: 'Напишите свое пожелание',
        character: 'Выберите героя',
        street: 'Улица',
        city: 'Город',
        country: 'Страна',
        zip_code: 'Почтовый индекс',
        select_country: 'Выберите страну',
        promocode: 'Промокод'
      },
      validation: {
        fill_language: {
          text: 'Заполните поле {lang} буквами.',
          ru: 'русскими',
          kk: 'казахскими ',
          latin: 'латинскими'
        },
        min: 'Не менее {count} символов',
        max: 'Не более {count} символов',
        required: 'Поле {field} необходимо заполнить.',
        required_character: 'Нужно выбрать своего героя.',
        string: 'Поле {field} должно быть строкой.'
      },
      languages: {
        ru: 'Русский',
        kk: 'Қазақша'
      },
      create_book: 'Создать книгу',
      copyright: '© Все права защищены.',
      continue: 'Продолжить',
      close: 'Закрыть',
      edit: 'Изменить',
      share: 'Поделиться',
      notice: {
        namebook: {
          title: '+1 счастливый ребенок',
          content: 'Еще одна волшебная книга нашла своего хозяина!'
        }
      },
      create_look: 'Создайте свою книжку!',
      payType: {
        once: 'оплатить <span class="text-warning">{sum}</span> сейчас',
        credit: 'оплатить <span class="text-warning">{sum}</span> сейчас, остальное в рассрочку на <span class="text-warning">{months} месяца</span>'
      },
      agree_oferta: 'Нажимая на кнопку "купить", Вы подтверждаете свое совершеннолетие и согласие на  <a target="_blank" href="/publichnaya-oferta">Публичную оферту</a>',
      promocode_status: {
        error: 'Промокод недействителен',
        success: 'Промокод действителен'
      },
      installment: {
        title: 'Рассрочка на',
        button: '3 месяца'
      },
      charity: 'Ваша помощь детскому фонду',
      delivery_date: 'Ожидаемое время доставки'
    }
    )
  })
}
