// eslint-disable-next-line nuxt/no-cjs-in-config
const axios = require('axios')
require('dotenv').config()
// const baseUrl = 'http://localhost:8000/api/'
const baseUrl = process.env.AXIOS_BASE_URL + '/api/'

// eslint-disable-next-line no-extend-native

export default {
  mode: 'universal',
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0', // default: localhost,
    timing: false
  },
  head: {
    title: process.env.TITLE,
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'
      },
      {
        'http-equiv': 'ScreenOrientation',
        content: 'autoRotate:disabled'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.DESCRIPTION
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: process.env.KEYWORDS
      },
      {
        hid: 'msapplication-TileColor',
        name: 'msapplication-TileColor',
        content: '#000000'
      },
      {
        hid: 'theme-color',
        name: 'theme-color',
        content: '#ffffff'
      },
      {
        hid: 'msapplication-config',
        name: 'msapplication-config',
        content: '/favicon/browserconfig.xml'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: '/favicon/og-image.jpg'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: '/favicon/og-image.jpg'
      },
      {
        hid: 'og:image:width',
        property: 'og:image:width',
        content: '256'
      },
      {
        hid: 'og:image:height',
        property: 'og:image:height',
        content: '256'
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: process.env.TITLE
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: process.env.DESCRIPTION
      },
      {
        hid: 'og:url',
        property: 'og:url',
        content: process.env.BASE_URL
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: process.env.SITE_NAME
      }
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/favicon/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png'
      },
      {
        rel: 'manifest',
        href: '/favicon/site.webmanifest'
      },
      {
        rel: 'mask-icon',
        href: '/favicon/safari-pinned-tab.svg',
        color: '#29b6f6'
      },
      {
        rel: 'shortcut icon',
        href: '/favicon/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,600,600i,700,700i&display=swap&subset=cyrillic,cyrillic-ext,latin-ext'
      }
    ]
  },

  /*
    ** Customize the progress-bar color
    */
  // loading: '~/components/Loading.vue',
  loading: {
    color: '#29b6f6',
    height: '3px'
  },
  /*
    ** Global CSS
    */
  css: [
    '@/assets/scss/app.scss'
  ],
  /*
    ** Plugins to load before mounting the App
    */
  plugins: [
    {
      src: '~/plugins/underscore',
      ssr: false
    },
    '~/plugins/axios.js',
    '~/plugins/i18n.js',
    '~/plugins/filters.js',
    '~/plugins/progressive-image.js',
    '~/plugins/slick-carousel.js',
    '~/plugins/mask.js',
    '~/plugins/uuid.js',
    {
      src: '~/plugins/outside-click.js',
      ssr: false
    },
    {
      src: '~/plugins/book.js',
      ssr: false
    },
    {
      src: '~/plugins/youtube.js',
      ssr: false
    }
  ],
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'namebooks-id-edit',
        path: '/namebooks/:id/edit',
        component: resolve(__dirname, 'pages/namebooks/_id/preview/index.vue')
      })
    }
  },
  /*
    ** Nuxt.js dev-modules
    */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
    ** Nuxt.js modules
    */
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    'nuxt-i18n',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/device',
    '@nuxtjs/dayjs',
    [
      'nuxt-vuex-localstorage',
      {
        localStorage: ['cart', 'book', 'currency']
      }
    ],
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '70747660',
        webvisor: true
      }
    ],
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-162176515-1'
      }
    ]
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  dayjs: {
    locales: ['ru', 'kk'],
    defaultLocale: 'ru',
  },
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false
  },
  /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
  axios: {
    baseURL: baseUrl,
    browserBaseURL: '/api',
    https: true,
    progress: false,
    credentials: false
  },
  i18n: {
    // baseUrl: 'https://my-nuxt-app.com',
    seo: true,
    parsePages: false,
    detectBrowserLanguage: false,
    pages: {
      404: {
        kk: false
      }
    },
    locales: [
      {
        code: 'ru',
        iso: 'ru-RU',
        name: 'Русский',
        file: 'ru.js'
      }, {
        code: 'kk',
        iso: 'kk-KZ',
        name: 'Қазақша',
        file: 'kk.js'
      }
    ],
    defaultLocale: 'ru',
    vueI18n: {
      fallbackLocale: 'ru'
    },
    lazy: true,
    langDir: 'locales/'
  },
  proxy: {
    '/api': {
      target: baseUrl,
      pathRewrite: {
        '^/api': '/'
      }
    }
  },
  robots: {
    UserAgent: '*',
    Disallow: '/',
    Sitemap: '/sitemap.xml'
  },
  sitemap: {
    hostname: process.env.BASE_URL,
    path: '/sitemap.xml',
    gzip: true,
    routes: async () => {
      const { data } = await axios.get(baseUrl + 'sitemap')
      return data
    }
  },
  build: {
    extend (config, ctx) {
    }
  }
}
